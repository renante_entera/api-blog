FactoryBot.define do
  factory :comment do
    commentable_type { "MyString" }
    commentable_id { 1 }
    user { nil }
    body { "MyText" }
  end
end
