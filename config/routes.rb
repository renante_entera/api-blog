Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :posts, only: [:index, :show, :create, :update, :destroy], param: :slug do
        resources :comments, only: [:index]
      end
      resources :users, only: [:create]

      post 'login', to: "auth#login"
      get 'my-posts', to: "posts#my_posts"
    end
  end
end
