class Post < ApplicationRecord
  after_validation :set_slug, only: %i[create update]

  belongs_to :user
  has_many :comments, as: :commentable, dependent: :destroy
  validates :title, :image, :content, presence: true

  def to_param
    "#{id}-#{slug}"
  end

  private

  def set_slug
    self.slug = title.to_s.parameterize
  end
end
