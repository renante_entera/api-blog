class User < ApplicationRecord
  after_validation :full_name

  attribute :full_name
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates :first_name, :last_name, :email, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  has_secure_password

  def full_name
    "#{first_name} #{last_name}"
  end
end
