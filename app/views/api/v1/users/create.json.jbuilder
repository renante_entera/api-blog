json.data do
  json.id @user.id
  json.first_name @user.first_name
  json.last_name @user.last_name
  json.email @user.email    
  json.updated_at @user.updated_at.strftime("%Y-%m-%d %H:%M:%S")
  json.created_at @user.created_at.strftime("%Y-%m-%d %H:%M:%S")
end