json.data @posts do |post|
  json.id post.id
  json.user post.user
  json.title post.title
  json.slug post.slug
  json.image post.image
  json.content post.content
  json.to_param post.to_param
  json.comments post.comments.count
  json.created_at post.created_at.strftime("%Y-%m-%d %H:%M:%S")
  json.updated_at post.updated_at.strftime("%Y-%m-%d %H:%M:%S")
end