class Api::V1::CommentsController < Api::V1::BaseController
  before_action :authorized, except: %i[index]
  before_action :set_commentable

  def index; end

  private

  def set_commentable
    @commentable = Post.find_by_id(params[:post_slug].to_i)
  end
end
