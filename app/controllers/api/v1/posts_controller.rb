class Api::V1::PostsController < Api::V1::BaseController
  before_action :authorized, except: %i[index show]
  before_action :set_post, only: %i[show update destroy]
  before_action :render_not_found, only: %i[show update destroy]
  before_action :require_same_user, only: %i[update destroy]

  def index
    @posts = Post.all.order(:created_at).reverse
  end

  def show; end

  def create
    @post = Post.new(post_params)
    @post.user = logged_in_user

    return if @post.save

    render_json_response(
      { message: "The given data was invalid.", errors: @post.errors },
      :unprocessable_entity
    )
  end

  def update
    return if @post.update(post_params)
  end

  def destroy
    @post.destroy

    render_json_response(
      { status: "record deleted successfully" },
      :ok
    )
  end

  def my_posts
    @posts = logged_in_user.posts.order(:created_at).reverse

    render :index
  end

  private

  def set_post
    @post = Post.find_by_id(params[:slug].to_i)
  end

  def post_params
    params.require(:post).permit(:title, :image, :content)
  end

  def render_not_found
    return if @post.present?

    render_json_response(
      { message: "No query results for model [App\\Post]." },
      :not_found
    )
  end

  # Update/delete post restriction
  def require_same_user
    return if logged_in_user == @post.user

    render_json_response(
      { message: "You can only update or delete your own post" },
      :forbidden
    )
  end
end
