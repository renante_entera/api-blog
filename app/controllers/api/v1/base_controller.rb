class Api::V1::BaseController < ApplicationController
  def render_json_response(json, status)
    render json: json, status: status
  end
end
