class Api::V1::UsersController < Api::V1::BaseController
  wrap_parameters :user, include: User.attribute_names + ["password", "password_confirmation"]

  def create
    @user = User.new(user_params)
    return if @user.save

    render json:   { message: "The given data was invalid.", errors: @user.errors },
           status: :unprocessable_entity
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
end
