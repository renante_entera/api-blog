class Api::V1::AuthController < Api::V1::BaseController
  before_action :render_error, only: %i[login]

  def login
    @user = User.find_by(email: params[:email].downcase)

    if @user&.authenticate(params[:password])
      @exp = 24.hours.from_now.to_i
      @token = encode_token({ user_id: @user.id, exp: @exp })
    else
      invalid({ email: ["These credentials do not match our records."] })
    end
  end

  private

  def validation(field)
    "The #{field} field is required." if params[field].blank?
  end

  def render_error
    errors = {}
    errors["email"] = [validation("email")] if validation("email").present?
    errors["password"] = [validation("password")] if validation("password").present?

    invalid(errors) unless errors.empty?
  end

  def invalid(error_messages)
    render json:   {
      message: "The given data was invalid.",
      errors:  error_messages
    },
           status: :unprocessable_entity
  end
end
